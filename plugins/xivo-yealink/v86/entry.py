# -*- coding: utf-8 -*-

# Copyright (C) 2013-2023 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os

common_globals = {}
execfile_('common.py', common_globals)

MODEL_VERSIONS = {
    u'T31P': u'124.86.0.75',
    u'T31W': u'124.86.0.75',
    u'T33G': u'124.86.0.75',
    u'T34W': u'124.86.0.75',
    u'T42U': u'108.86.0.90',
    u'T43U': u'108.86.0.90',
    u'T44U': u'108.86.0.90',
    u'T44W': u'108.86.0.90',
    u'T46U': u'108.86.0.90',
    u'T48U': u'108.86.0.90',
    u'T53': u'96.86.0.70',
    u'T53W': u'96.86.0.70',
    u'T54W': u'96.86.0.70',
    u'T57W': u'96.86.0.70',

}
COMMON_FILES = [
    # voir Yealink SIP IP Phones Auto Provisioning Guide V1.1.pdf dans la page du modèle sur http://support.yealink.com/documentFront/forwardToDocumentFrontDisplayPage
    ('y000000000123.cfg', u'T31(T30,T30P,T31G,T31P,T33P,T33G)-124.86.0.75.rom', 'model.tpl'),  # T31P
    ('y000000000172.cfg', u'T31(T30,T30P,T31G,T31P,T33P,T33G)-124.86.0.75.rom', 'model.tpl'),  # T31W
    ('y000000000124.cfg', u'T31(T30,T30P,T31G,T31P,T33P,T33G)-124.86.0.75.rom', 'model.tpl'),  # T33G
    ('y000000000171.cfg', u'T31(T30,T30P,T31G,T31P,T33P,T33G)-124.86.0.75.rom', 'model.tpl'),  # T34W
    ('y000000000116.cfg', u'T46U(T43U,T46U,T41U,T48U,T42U,T44U,T44W)-108.86.0.90.rom', 'model.tpl'), #T42U
    ('y000000000107.cfg', u'T46U(T43U,T46U,T41U,T48U,T42U,T44U,T44W)-108.86.0.90.rom', 'model.tpl'), #T43U
    ('y000000000173.cfg', u'T46U(T43U,T46U,T41U,T48U,T42U,T44U,T44W)-108.86.0.90.rom', 'model.tpl'), #T44U 
    ('y000000000174.cfg', u'T46U(T43U,T46U,T41U,T48U,T42U,T44U,T44W)-108.86.0.90.rom', 'model.tpl'), #T44W 
    ('y000000000108.cfg', u'T46U(T43U,T46U,T41U,T48U,T42U,T44U,T44W)-108.86.0.90.rom', 'model.tpl'), #T46U
    ('y000000000109.cfg', u'T46U(T43U,T46U,T41U,T48U,T42U,T44U,T44W)-108.86.0.90.rom', 'model.tpl'), #T48U
    ('y000000000095.cfg', u'T54W(T57W,T53W,T53)-96.86.0.70.rom', 'model.tpl'),  # T53 & T53W
    ('y000000000096.cfg', u'T54W(T57W,T53W,T53)-96.86.0.70.rom', 'model.tpl'),  # T54W
    ('y000000000097.cfg', u'T54W(T57W,T53W,T53)-96.86.0.70.rom', 'model.tpl'),  # T57W
 ]

COMMON_FILES_DECT = None

COMMON_ROM_FILES_DECT = None

class YealinkPlugin(common_globals['BaseYealinkPlugin']):
    IS_PLUGIN = True

    pg_associator = common_globals['BaseYealinkPgAssociator'](MODEL_VERSIONS)

    # Yealink plugin specific stuff

    _COMMON_FILES = COMMON_FILES

    def configure_common(self, raw_config):
        super(YealinkPlugin, self).configure_common(raw_config)