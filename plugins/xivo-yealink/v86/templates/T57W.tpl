{% extends 'base.tpl' -%}

{% block model_specific_parameters -%}
phone_setting.backgrounds = xivologo.png
phone_setting.idle_dsskey_and_title.transparency = 20%
wallpaper_upload.url = http://{{ ip }}:8667/xivologo.png

phone_setting.idle_clock_display.enable=0

phone_setting.virtual_keyboard.enable=1
gui_onscreen_keyboard.url=http://{{ ip }}:8667/keyboard/keyboard_lang.xml
gui_onscreen_keyboard.url=http://{{ ip }}:8667/keyboard/keyboard_ime_francais.xml
gui_onscreen_keyboard.url=http://{{ ip }}:8667/keyboard/keyboard_ime_num.xml
gui_onscreen_keyboard.url=http://{{ ip }}:8667/keyboard/keyboard_layout_francais.xml
gui_onscreen_keyboard.url=http://{{ ip }}:8667/keyboard/keyboard_layout_2.xml
{% endblock %}
