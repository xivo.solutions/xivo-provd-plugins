# -*- coding: utf-8 -*-

# Copyright (C) 2013-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os

common_globals = {}
execfile_('common.py', common_globals)

MODEL_VERSIONS = {
    u'T31P': u'124.85.0.40',
    u'T33G': u'124.85.0.40',
    u'T53': u'96.85.0.5',
    u'T54W': u'96.85.0.5',
    u'T57W': u'96.85.0.5',
    u'W70B': u'146.85.0.20',
    u'W60B': u'77.85.0.25',
}
COMMON_FILES = [
    # voir Yealink SIP IP Phones Auto Provisioning Guide V1.1.pdf dans la page du modèle sur http://support.yealink.com/documentFront/forwardToDocumentFrontDisplayPage
    ('y000000000123.cfg', u'T31(T30,T30P,T31G,T31P,T33P,T33G)-124.85.0.40.rom', 'model.tpl'),  # T31P
    ('y000000000124.cfg', u'T31(T30,T30P,T31G,T31P,T33P,T33G)-124.85.0.40.rom', 'model.tpl'),  # T33G
    ('y000000000095.cfg', u'T54W(T57W,T53W,T53)-96.85.0.5.rom', 'model.tpl'),  # T53
    ('y000000000096.cfg', u'T54W(T57W,T53W,T53)-96.85.0.5.rom', 'model.tpl'),  # T54W
    ('y000000000097.cfg', u'T54W(T57W,T53W,T53)-96.85.0.5.rom', 'model.tpl'),  # T57W
]

COMMON_FILES_DECT = [
    ('y000000000146.cfg', u'W70B-146.85.0.20.rom', u'W73H-116.85.0.15.rom', 'model_dect.tpl'),  # W73P
    ('y000000000077.cfg', u'W60B-77.85.0.25.rom', u'W56H-61.85.0.20.rom', 'model_dect.tpl'),  # W60P
]

COMMON_ROM_FILES_DECT = {
    u'W56H_fw_handset_filename': u'W56H-61.85.0.35.rom',
    u'W53H_fw_handset_filename': u'W53H-88.85.0.20.rom',
    u'W73H_fw_handset_filename': u'W73H-146.85.0.15.rom',
    u'W59R_fw_handset_filename': u'W59R-115.85.0.35.rom',
}


class YealinkPlugin(common_globals['BaseYealinkPlugin']):
    IS_PLUGIN = True

    pg_associator = common_globals['BaseYealinkPgAssociator'](MODEL_VERSIONS)

    # Yealink plugin specific stuff

    _COMMON_FILES = COMMON_FILES

    def configure_common(self, raw_config):
        super(YealinkPlugin, self).configure_common(raw_config)
        for filename, fw_filename, fw_handset_filename, tpl_filename in COMMON_FILES_DECT:
            tpl = self._tpl_helper.get_template('common/%s' % tpl_filename)
            dst = os.path.join(self._tftpboot_dir, filename)
            raw_config[u'XX_fw_filename'] = fw_filename
            raw_config[u'XX_fw_handset_filename'] = fw_handset_filename
            raw_config['W56H_fw_handset_filename'] = COMMON_ROM_FILES_DECT['W56H_fw_handset_filename']
            raw_config['W53H_fw_handset_filename'] = COMMON_ROM_FILES_DECT['W53H_fw_handset_filename']
            raw_config['W73H_fw_handset_filename'] = COMMON_ROM_FILES_DECT['W73H_fw_handset_filename']
            raw_config['W59R_fw_handset_filename'] = COMMON_ROM_FILES_DECT['W59R_fw_handset_filename']
            self._tpl_helper.dump(tpl, raw_config, dst, self._ENCODING)
