{% extends 'base.tpl' %}

{%- block dnd_param %}
    <gui_fkey4 perm="R">none</gui_fkey4>
{% endblock -%}

{% block fkeys_prefix %}
{% if XX_xivo_phonebook_url %}
    <fkey idx="4" context="active" perm="R">url {{ XX_xivo_phonebook_url|e }}</fkey>
{% endif %}
{% endblock %}
